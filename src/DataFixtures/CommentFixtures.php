<?php

namespace App\DataFixtures;
use App\Entity\Restaurant;
use App\Entity\Comment;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;


class CommentFixtures extends BaseFixture implements DependentFixtureInterface
{
    protected function loadData(ObjectManager $manager)
    {

        $this->createMany(100, 'main_comments', function() {

            $comment = new Comment();
            $comment->setContent(
                $this->faker->boolean ? $this->faker->paragraph : $this->faker->sentences(2, true)
            );
            $comment->setCreatedAt($this->faker->dateTimeBetween('-1 months', '-1 seconds'));
            $comment->setIsDeleted($this->faker->boolean(20));
            $comment->setRestaurant($this->getRandomReference('main_restaurant'));
            $comment->setAuthor($this->getRandomReference('main_users'));
            $comment->setStars($this->faker->numberBetween(1,4));
            return $comment;
        });

        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {

        return [
            RestaurantFixtures::class,
            UserFixtures::class
        ];
    }
}
