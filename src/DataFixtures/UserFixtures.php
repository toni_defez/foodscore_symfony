<?php

namespace App\DataFixtures;


use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends BaseFixture
{
    private $passwordEncoder;

    private static $userImages = [
        'alien-profile.png',
        'astronaut-profile.png'
    ];

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(10, 'main_users',
            function($i) use ($manager) {
            $user = new User();
            $user->setEmail($this->faker->email);
            $user->setAvatar($this->faker->randomElement(self::$userImages));
            $user->setNickname($this->faker->firstName);
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                'password'
            ));
            return $user;
        });


        $this->createMany(3, 'admin_users', function($i) {
            $user = new User();
            $user->setEmail(sprintf('admin%d@gmail.com', $i));
            $user->setNickname($this->faker->firstName);
            $user->setRoles(['ROLE_ADMIN']);
            $user->setAvatar($this->faker->randomElement(self::$userImages));
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                'engage'
            ));
            return $user;
        });
        /**
       $this->createMany(User::class,10,
           function (User $user, $count){
                $user->setEmail($this->faker->email);
                $user->setNickname($this->faker->name);
               $user->setPassword($this->passwordEncoder->encodePassword(
                   $user,
                   'password'
               ));
            }
       );

        $this->createMany(User::class,1,
            function (User $user, $count){
                $user->setEmail("admin1@gmail.com");
                $user->setNickname($this->faker->name);
                $user->setRoles(['ROLE_ADMIN']);
                $user->setPassword($this->passwordEncoder->encodePassword(
                    $user,
                    'password'
                ));
            }
        );
         * **/
        $manager->flush();
    }
}
