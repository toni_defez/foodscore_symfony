<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Restaurant;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class RestaurantFixtures extends BaseFixture implements DependentFixtureInterface
{
    private static $restaurantNames = [
        'Vips',
        'The good Burger',
        'MacDonalds',
        'Burger King',
        'El caldero',
        'Casa Riquelme',
        'El Sorell',
        'El chino del barrio '
    ];

    private static $articleImages = [
        'rest1.jpg',
        'rest2.jpg',
        'rest3.jpg',
        'rest4.jpg',
        'rest5.jpg',
        'rest6.jpg'
    ];

    private static $cuisineRestaurant = [
        'cocina inglesa',
        'Cocina francesa',
        'Comida Rapida',
        'Comida India',
        'Comida gourmet',
        'China'
    ];

    private static $ownerRestaurant = [
        'Mike Ferengi',
        'Amy Oort',
        'Fredy Mercury',
        'El gallego loco'
    ];

    private static $daysRestaurants = [
        '1,2,3,4',
        '0,1,4,5',
        '2,3,4,6',
        '1,4,6',
        '2,4,6',
        '1,3,5',
        '1,6',
        '2,4,5,6'
    ];



    protected function loadData(ObjectManager $manager)
    {

        $this->createMany(100, 'main_restaurant',
            function($count) use ($manager) {
                $restaurant = new Restaurant();
                $restaurant
                    ->setName($this->faker->name)
                    ->setCuisine($this->faker->randomElement(self::$cuisineRestaurant))
                    ->setDaysOpen($this->faker->randomElement(self::$daysRestaurants))
                    ->setDescription($this->faker->text(30))
                    ->setImage($this->faker->randomElement(self::$articleImages))
                    ->setPhone($this->faker->phoneNumber)
                    ->setHeartCount($this->faker->numberBetween(5,100))
                    ->setStars($this->faker->numberBetween(1,5))
                    ->setIsActive($this->faker->boolean(80))
                    ->setPublishAt(
                        $this->faker->dateTimeBetween('-100 days','-1 days'));

                $restaurant->setCategory(
                    $this->getRandomReference("main_category"));
                $restaurant->setOwner(
                  $this->getRandomReference("main_users")
                );
                return $restaurant;

            });


        $manager->flush();

    }


    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            CategoryFixtures::class,
            UserFixtures::class
        ];
    }
}
