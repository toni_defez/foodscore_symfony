<?php
/**
 * Created by PhpStorm.
 * User: tonidefez
 * Date: 2019-02-09
 * Time: 20:54
 */

namespace App\Form;


use App\Entity\Category;
use App\Entity\Restaurant;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class RestaurantFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class)
            ->add('description')
            ->add('phone',TelType::class)
            ->add('cuisine')
            ->add('arraydaysarray', ChoiceType::class, [
                'choices' => [
                    'Monday' => 0,
                    'Tuesday' => 1,
                    'Wednesday' => 2,
                    'Thursday' => 3,
                    'Friday'=>4,
                    'Saturday'=>5,
                    'Sunday'=>6
                ],
                'expanded'  => true,
                'multiple'  => true,
                'label_attr' => array(
                    'class' => 'checkbox-inline'
                    ),
               'label'=>"Select open days"
            ])
            ->add('category'
            ,EntityType::class,[
                'class'=>Category::class,
                'placeholder'=>'Chose a category',


                ])
            ->add( 'image',
                FileType::class,
                [
                   'label' => 'Imagen (PNG/JPG)',
                    'data_class' => null
                ])
        ;

    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Restaurant::class
        ]);
    }



}