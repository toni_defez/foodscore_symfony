<?php

namespace App\Controller;


use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Restaurant;
use App\Form\CommentFormType;
use App\Form\RestaurantFormType;
use App\Helpers\FileUploader;
use App\Repository\CategoryRepository;
use App\Repository\RestaurantRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\File;

class RestaurantController extends AbstractController
{
    /**
     * @Route("/", name="app_homepage")
     */
    public function listAllRestaurants(RestaurantRepository $repositoryRestaurant,
                                       CategoryRepository $repositoryCategory,Request $request,
                                       PaginatorInterface $paginator)
    {


       // $restaurants =$repository->findBy([],['publisAt'=>'DESC']);
        /*
        $restaurants =$repositoryRestaurant->findAll();
        $categories = $repositoryCategory->findAll();
            //$repository->findAllOpen(2);
        return $this->render('restaurants/index.html.twig',[
            'restaurants'=>$restaurants,
            'categories'=>$categories
        ]);
        */
        $name = $request->query->get('name')??"";
        $categoria = $request->query->get('categoria')??"";
        $numberStar = $request->query->get('numberStar')??"";
        $open = $request->query->get('open')??"";
        $active = $request->query->get('active')??"";
        if(!$this->isGranted('ROLE_ADMIN')){
            $active = "Yes";
        }

        dump("name:".$name);
        dump("categoria:".$categoria);
        dump('estrellas'.$numberStar);
        dump("open".$open);



        // $restaurants =$repository->findBy([],['publisAt'=>'DESC']);
        $queryBuilder =$repositoryRestaurant->getWithSearchQueryBuilder($categoria,$numberStar,$name,$open,$active);

        $categories = $repositoryCategory->findAll();

        $pagination = $paginator->paginate(
            $queryBuilder, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('restaurants/index.html.twig', [
            'pagination'=>$pagination,
            'categories'=>$categories
        ]);
    }

    /**
     * @Route("/add_restaurant", name="add_restaurant")
     * @IsGranted("ROLE_USER")
     */
    public function newRestaurant(EntityManagerInterface $em,
                                  Request $request,
                                  FileUploader $fileUploader){
        $form = $this->createForm(RestaurantFormType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $restaurant = $form->getData();

            /** @var UploadedFile $image */
            $image = $form->get('image')->getData();
            $fileName = $fileUploader->upload($image);

            $restaurant->setImage($fileName);
            $restaurant
                ->setOwner($this->getUser())
                ->setPublishAt(
                    new \DateTime());

            $em->persist($restaurant);
            $em->flush();
            $this->addFlash('success','New restaurant added');
            return $this->redirectToRoute('app_homepage');

        }

        return $this->render('restaurants/add_restaurant.html.twig', [
            'restaurantForm'=>$form->createView()
        ]);
    }

    /**
     * @Route("/restaurant/{slug}",name="restaurant_show")
     */
    public function detailRestaurant(Restaurant $restaurant, EntityManagerInterface $em,Request $request
    ,RestaurantRepository $repository){

        $formComment = $this->createForm(CommentFormType::class);
        $formComment->handleRequest($request);

        if ($formComment->isSubmitted() && $formComment->isValid()) {

            /** @var Comment $comment */
            $comment = $formComment->getData();
            $comment->setAuthor($this->getUser());
            $comment->setRestaurant($restaurant);
            $em->persist($comment);
            $em->flush();
            $puntacion = $repository->CurrentStars($restaurant);
            $restaurant->setStars($puntacion['rating']);
            $em->persist($restaurant);
            $em->flush($restaurant);

            $this->addFlash('success', 'Comment published');
        }

        return $this->render('restaurants/detail_restaurant.html.twig',
            [
                'restaurant'=>$restaurant,
                'form'=>$formComment->createView()
            ]);
    }




    /**
     * @Route("/restaurant/{slug}/heart", name="restaurant_heart",methods={"GET"})
     */
    public function likeRestaurant(Restaurant $restaurant,LoggerInterface $logger,
                                    EntityManagerInterface $em){
        $logger->info("Un restaurante ha recibido un like");

        $restaurant->incrementHeartCount();
        $em->flush();

        echo json_encode("DIOS");

    }

    /**
     * @Route("/restaurant/{id}/edit",name="restaurant_edit")
     * @IsGranted("ROLE_USER")
     */
    public function edit(Restaurant $restaurant,
                         Request $request,
                         FileUploader $fileUploader,
                         EntityManagerInterface $em)
    {

        $this->denyAccessUnlessGranted('MANAGE',$restaurant);
        $restaurant->setImage(
            new File($this->getParameter('images_directory').'/restaurants/'.$restaurant->getImage())
        );
        $array = explode(',', $restaurant->getDaysOpen());
        $restaurant->setArrayDaysArray($array);

        $form = $this->createForm(RestaurantFormType::class, $restaurant);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $restaurant = $form->getData();

            /** @var UploadedFile $image */
            $image = $form->get('image')->getData();
            $fileName = $fileUploader->upload($image);
            $restaurant->setImage($fileName);


            $em->persist($restaurant);
            $em->flush();
            $this->addFlash('success', 'Restaurant updated');
            return $this->redirectToRoute('app_homepage');
        }
        return $this->render('restaurants/edit_restaurant.html.twig', [
            'restaurantForm'=>$form->createView()
        ]);
    }
    /**
     * @Route("/restaurant/{id}/delete",name="restaurant_delete")
     * @IsGranted("ROLE_USER")
     */
    public function delete(bool $borrado=true,Restaurant $restaurant){

        if (is_null($restaurant))
            throw new NotFoundHttpException('No se ha encontrado el contacto buscado');

        $em = $this->getDoctrine()->getManager();
        $restaurant->setIsActive(false);
        $em->persist($restaurant);
        $em->flush();
        return new JsonResponse(["true"=>true]);

    }


    /**
     * @Route("/restaurant/{id}/activate",name="restaurant_activate")
     * @IsGranted("ROLE_USER")
     */
    public function activate(bool $borrado=true,Restaurant $restaurant){

        if (is_null($restaurant))
            throw new NotFoundHttpException('No se ha encontrado el contacto buscado');

        $em = $this->getDoctrine()->getManager();
        $restaurant->setIsActive(true);
        $em->persist($restaurant);
        $em->flush();

        return new JsonResponse(["true"=>true]);

    }





}
