<?php
/**
 * Created by PhpStorm.
 * User: tonidefez
 * Date: 2019-02-04
 * Time: 14:24
 */

namespace App\Controller;


use App\Entity\Restaurant;
use App\Entity\User;
use App\Form\AdminFormType;
use App\Repository\CommentRepository;
use App\Repository\RestaurantRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @IsGranted({"ROLE_ADMIN","ROLE_MANAGER"})
 */
class AdminController extends  AbstractController
{


    /**
     *@Route("/admin/restaurants",name="restaurant_admin")
     */
    public function list(RestaurantRepository $restaurantRepository)
    {
        $restaurants = $restaurantRepository->findAll();
        return $this->render('admin/restaurantAdmin.html.twig', [
            'restaurants' => $restaurants,
        ]);
    }


    /**
     *@Route("/admin/users",name="users_admin")
     */
    public function listUser(UserRepository $repositoryUser, PaginatorInterface $paginator,Request $request){
        $q = $request->query->get('q');
        $queryBuilder = $repositoryUser->getAllUser($q);

        $pagination = $paginator->paginate(
            $queryBuilder, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            9/*limit per page*/
        );

        return $this->render('admin/userAdmin.html.twig', [
            'pagination'=>$pagination
        ]);
    }

    /**
     * @Route("/admin/{id}/edit",name="user_edit_admin")
     * @IsGranted("ROLE_ADMIN")
     */
    public function changeProfileAdmin(User $user,Request $request,EntityManagerInterface $em){
        $form = $this->createForm(AdminFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $rol = $form->get('rol')->getData();

            if($rol == "ROLE_USER") {
                $user->setRoles([]);
            }
            else {
                $user->setRoles([$rol]);
            }
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', 'Profile updated');
        }

        return $this->render('account/edit_user.html.twig', [
            'user'=>$user,
            'edit'=>" Profile",
            'form'=>$form->createView()
        ]);

    }


    /**
     * @Route("admin/comments", name="comment_admin")
     */
    public function index(CommentRepository $repository, Request $request,
                          PaginatorInterface $paginator)
    {
        $q = $request->query->get('q');

        $queryBuilder = $repository->getWithSearchQueryBuilder($q);

        $pagination = $paginator->paginate(
            $queryBuilder, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('admin/commentAdmin.html.twig', [
            'pagination'=>$pagination
        ]);
    }
}