<?php
/**
 * Created by PhpStorm.
 * User: tonidefez
 * Date: 2019-02-09
 * Time: 20:29
 */

namespace App\Controller;


use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class BaseController extends  AbstractController
{
    protected function getUser():User
    {
        return parent::getUser();
    }

}