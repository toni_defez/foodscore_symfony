<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\EditAvatarType;
use App\Form\EditPasswordType;
use App\Form\EditProfileTypePhpType;
use App\Helpers\FileUploader;
use App\Repository\RestaurantRepository;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @IsGranted("ROLE_USER")
 */
class AccountController extends BaseController
{



    /**
     * @Route("/account/{id}", name="app_account_user")
     */
    public function accountSelf(User $user,RestaurantRepository $repository, PaginatorInterface $paginator,
                            Request $request)
    {
        $queryBuilder = $repository->getRestaurantByOwner($user);
        $pagination = $paginator->paginate(
            $queryBuilder, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('account/index.html.twig', [
            'user'=>$user,
            'pagination'=>$pagination,
        ]);
    }


    /**
     *@Route("/account/{id}/edit_name",name="app_edit_name")
     */
    public function editName(User $user,Request $request,EntityManagerInterface $em){
        $this->denyAccessUnlessGranted('MANAGE',$user);

        $form = $this->createForm(EditProfileTypePhpType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', 'Profile updated');
        }

        return $this->render('account/edit_user.html.twig', [
            'user'=>$user,
            'edit'=>'profile',
            'form'=>$form->createView()
        ]);

    }

    /**
     *@Route("/account/{id}/edit_password",name="app_edit_password")
     */
    public function editPassword(User $user,Request $request, UserPasswordEncoderInterface $passwordEncoder,
        EntityManagerInterface $em){
        $this->denyAccessUnlessGranted('MANAGE',$user);
        $form = $this->createForm(EditPasswordType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $user->getPassword()
                )
            );
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', 'Password updated');
        }

        return $this->render('account/edit_user.html.twig', [
            'user'=>$user,
            'edit'=>'password',
            'form'=>$form->createView()
        ]);
    }

    /**
     *@Route("/account/{id}/edit_photo",name="app_edit_photo")
     */
    public function editPhoto(User $user,FileUploader $fileUploader,Request $request,EntityManagerInterface $em){
        $this->denyAccessUnlessGranted('MANAGE',$user);
        $form = $this->createForm(EditAvatarType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $image = $form->get('avatar')->getData();
            $fileName = $fileUploader->upload($image,'users');
            $user->setAvatar($fileName);
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', 'Password updated');
        }
        return $this->render('account/edit_user.html.twig', [
            'user'=>$user,
            'edit'=>'Avatar',
            'form'=>$form->createView()
        ]);
    }

}
