<?php

namespace App\Controller;

use App\Entity\Message;
use App\Entity\User;
use App\Form\MessageType;
use App\Repository\MessageRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class MessageController extends BaseController
{
    /**
     * @Route("message/list/{id}", name="message_index")
     */
    public function index(User $user): Response
    {
        $this->denyAccessUnlessGranted('MANAGE',$user);
        return $this->render('message/index.html.twig', [
            'messages' => $user->getReceptor(),
        ]);
    }

    /**
     * @Route("message/new/{id}", name="message_new", methods={"GET","POST"})
     */
    public function new(User $user,Request $request,UserRepository $repository): Response
    {
        $this->denyAccessUnlessGranted('MANAGE',$user);

        $message = new Message();
        $message->setEmisor($this->getUser());
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        if ($form->isSubmitted()  && $form->isValid()) {

            if($message->getEmisor() !=null && $message->getReceptor() !=null) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($message);
                $entityManager->flush();
                $this->addFlash('success', 'Message send');
            }
            $this->addFlash('error', 'Message not send');
            return $this->redirectToRoute('message_index',array('id' => $user->getId()));
        }

        return $this->render('message/new.html.twig', [
            'message' => $message,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("message/{id}", name="message_show", methods={"GET"})
     */
    public function show(Message $message): Response
    {
        return $this->render('message/show.html.twig', [
            'message' => $message,
        ]);
    }

    /**
     * @Route("message/send/{id}",name="messageSend")
     */
    public function showSend(User $user):Response{
        return $this->render('message/index.html.twig', [
            'messages' => $user->getEmisor(),
        ]);
    }



    /**
     * @Route("message/{id}", name="message_delete")
     */
    public function delete(Request $request, Message $message): Response
    {
        if ($this->isCsrfTokenValid('delete'.$message->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($message);
            $entityManager->flush();
            $this->addFlash('success', 'Message deleted');
        }
        return $this->redirectToRoute('message_index',array('id' => $message->getEmisor()->getId()));
    }
}
