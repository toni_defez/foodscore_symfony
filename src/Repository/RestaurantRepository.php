<?php

namespace App\Repository;

use App\Entity\Restaurant;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Restaurant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Restaurant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Restaurant[]    findAll()
 * @method Restaurant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RestaurantRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Restaurant::class);
    }

     /**
      * @return Restaurant[] Returns an array of Restaurant objects
      */

    public function findAllOpen($value)
    {
        return $this
            ->addIsOpenQueryBuilder()
            ->innerJoin('r.category','c')
            ->addSelect('c')
            ->andWhere('r.stars >= 0')
            ->orderBy('r.publishAt', 'DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

    private function addIsOpenQueryBuilder(QueryBuilder $qb=null){
       // return $this->getOrCreateQueryBuilder($qb)
       //     ->andWhere('r.daysOpen LIKE  :val')
       //     ->setParameter('val','%'."1".'%');
        return $this->getOrCreateQueryBuilder($qb);
    }


    public function getRestaurantByOwner(User $user){

        $qb = $this->createQueryBuilder('r')
            ->innerJoin('r.owner','u')
            ->addSelect('u')
            ->orderBy('r.publishAt','DESC')
            ->andWhere('r.owner = :val')
            ->setParameter('val',$user);
        return $qb;
    }


    public  function  getWithSearchQueryBuilder(?string $categoria, ?string $numberStar,
                                                ?string $name, ?string $open, ?string $active){

        dump("en el repositorio");
        dump("name:->".$name);
        dump("categoria:->".$categoria);
        dump('estrellas->'.$numberStar);
        dump("open->".$open);
        dump ("active=>".$active);


        $qb = $this->createQueryBuilder('r')
            ->innerJoin('r.category','c')
            ->addSelect('c')
            ->orderBy('r.publishAt', 'DESC');

        if($open!="" && $open == "Yes"){
            $dayofweek = date('w', time())."";

            $qb->andWhere('r.daysOpen LIKE  :val1')
           ->setParameter('val1','%'.$dayofweek.'%');
        }

        if($categoria!=""  && $categoria !="-1"){
            $qb->andWhere('c.id = :val2 ')
                ->setParameter('val2',$categoria);
        }
        if($numberStar !="" && $numberStar!=0){
            $qb->andWhere('r.stars = :val3')
                ->setParameter('val3',$numberStar);
        }
        if($name!=""){
            $qb->andWhere('r.name LIKE :term OR r.cuisine LIKE :term')
                ->setParameter('term','%'.$name.'%');
        }
        if($active !== "Yes")
            $qb->andWhere('r.isActive = false');
        else
            $qb->andWhere('r.isActive = true');
        return $qb;
    }


    private  function getOrCreateQueryBuilder( QueryBuilder $qb=null){
        return $qb?:$this->createQueryBuilder('r');
    }

    public static function createNonDeletedCriteria(){
        return $criteria = Criteria::create()
            ->andWhere(Criteria::expr()->eq('isDeleted', false))
            ->orderBy(['createdAt' => 'DESC']);
    }

    /**
    public function getCurrentRating($restaurant){

        $select = "Select ROUND(avg(rating)) as rating from comment where id_restaurante =".$restaurant->getId();

       return $select;
    }
     * **/

    public function CurrentStars(Restaurant $restaurant){
        $conn = $this->getEntityManager()
            ->getConnection();

        $sql = "Select ROUND(avg(stars)) as rating from comment where restaurant_id =:id";
        $stmt = $conn->prepare($sql);
        $stmt->execute(['id'=>$restaurant->getId()]);

        return $stmt->fetch();
    }


    /*
    public function findOneBySomeField($value): ?Restaurant
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            -setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
