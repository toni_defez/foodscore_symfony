function iniciarEventos()
{
    let arrDeletes = document.querySelectorAll('.btn-delete');
    let arrActivate = document.querySelectorAll('.btn-secondary');

    arrDeletes.forEach(element => {
        element.addEventListener('click', deleteImage);
    });

    arrActivate.forEach(element=>{
       element.addEventListener('click',activateRestaurant) ;
    });
}


function activateRestaurant(event){
    event.preventDefault();

    let element = event.target;
    let id = element.getAttribute('id');

    let url = 'http://127.0.0.1:8000/restaurant/'+id+'/activate';

    fetch(url, {
        method: 'GET',
        headers: {},
        body: null
    }).then(response => {
        if (response.ok)
        {
            console.log("Tu puta madre");
            element.parentElement.parentElement.classList.remove('bg-warning','text-white');
            element.remove();
        }
        else
        {
            alert('No se ha podido borrar el elemento');
        }
    })
}

function deleteImage(event)
{

    event.preventDefault();

    let element = event.target;
    let id = element.getAttribute('id');

    let url = 'http://127.0.0.1:8000/restaurant/' +id+'/delete';

    fetch(url, {
        method: 'GET',
        headers: {},
        body: null
    }).then(response => {
        if (response.ok)
        {
            element.parentElement.parentElement.remove();
        }
        else
        {
            alert('No se ha podido borrar el elemento');
        }
    })
}

document.addEventListener("DOMContentLoaded", iniciarEventos);
